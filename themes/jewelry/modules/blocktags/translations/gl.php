<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocktags}jewelry>blocktags_f2568a62d4ac8d1d5b532556379772ba'] = 'Bloque de etiquetas';
$_MODULE['<{blocktags}jewelry>blocktags_73293a024e644165e9bf48f270af63a0'] = 'Numero non válido';
$_MODULE['<{blocktags}jewelry>blocktags_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activado';
$_MODULE['<{blocktags}jewelry>blocktags_b9f5c797ebbf55adccdd8539a65a0241'] = 'Desactivado';
$_MODULE['<{blocktags}jewelry>blocktags_c9cc8cce247e49bae79f15173ce97354'] = 'Gardar';
$_MODULE['<{blocktags}jewelry>blocktags_189f63f277cd73395561651753563065'] = 'Etiquetas';
$_MODULE['<{blocktags}jewelry>blocktags_49fa2426b7903b3d4c89e2c1874d9346'] = 'Máis sobre';
$_MODULE['<{blocktags}jewelry>blocktags_4e6307cfde762f042d0de430e82ba854'] = 'Ningunha etiqueta especificada';
$_MODULE['<{blocktags}jewelry>blocktags_70d5e9f2bb7bcb17339709134ba3a2c6'] = 'Ningunha etiqueta especificada';
