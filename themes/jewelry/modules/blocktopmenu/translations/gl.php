<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_efc226b17e0532afff43be870bff0de7'] = 'configuracion de actualizacion';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_bf24faeb13210b5a703f3ccef792b000'] = 'todos os fabricantes';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activado';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_b9f5c797ebbf55adccdd8539a65a0241'] = 'Desactivado';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_c9cc8cce247e49bae79f15173ce97354'] = 'Gardar';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_b021df6aac4654c454f46c77646e745f'] = 'Etiqueta';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_97e7c9a7d06eac006a28bf05467fcc8b'] = 'Enlace';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_e5dc8e5afea0a065948622039358de37'] = 'Nova fiestra';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_ec211f7c20af43e742bf2570c3cb84f9'] = 'Engadir';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_06933067aafd48425d67bcb01bba5cb6'] = 'Actualizar';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_ec136b444eede3bc85639fac0dd06229'] = 'Distribuidor';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_c0bd7654d5b278e65f21cf4e9153fdb4'] = 'Fabricante';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_af1b98adf7f686b84cd0b443e022b7a0'] = 'Categorias';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_12a521af593422cd508f7707662c9eb2'] = 'Boutiques';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_068f80c7519d0528fb08e82137a72131'] = 'Produtos';
$_MODULE['<{blocktopmenu}jewelry>blocktopmenu_e93c33bd1341ab74195430daeb63db13'] = 'Nome da Tenda';
$_MODULE['<{blocktopmenu}jewelry>form_1063e38cb53d94d386f21227fcd84717'] = 'Remover';
$_MODULE['<{blocktopmenu}jewelry>form_ec211f7c20af43e742bf2570c3cb84f9'] = 'Engadir';
