<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_a2e9cd952cda8ba167e62b25a496c6c1'] = 'Bloco de informação do utilizador';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_970a31aa19d205f92ccfd1913ca04dc0'] = 'Adiciona um bloco que mostra ao cliente registado ligações para gerir a conta.';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_0c3bf3014aafb90201805e45b5e62881'] = 'Ver o meu carrinho de compras';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_a85eba4c6c699122b2bb1387ea4813ad'] = 'Carrinho';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_deb10517653c255364175796ace3553f'] = 'Produto';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_068f80c7519d0528fb08e82137a72131'] = 'Produtos';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_9e65b51e82f2a9b9f72ebe3e083582bb'] = '(vazio)';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_2cbfb6731610056e1d0aaacde07096c1'] = 'Ver a minha conta de cliente';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_a0623b78a5f2cfe415d9dbbd4428ea40'] = 'A sua conta';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_83218ac34c1834c26781fe4bde918ee4'] = 'Bem-vindo(a)';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_4b877ba8588b19f1b278510bf2b57ebb'] = 'Terminar sessão';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_c87aacf5673fada1108c9f809d354311'] = 'Fechar sessão';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_d4151a9a3959bdd43690735737034f27'] = 'Inicie sessão na sua conta de cliente';
$_MODULE['<{blockuserinfo}jewelry>blockuserinfo_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Entrar';
$_MODULE['<{blockuserinfo}jewelry>nav_2cbfb6731610056e1d0aaacde07096c1'] = 'Ver a minha conta de cliente';
$_MODULE['<{blockuserinfo}jewelry>nav_4b877ba8588b19f1b278510bf2b57ebb'] = 'Terminar sessão';
$_MODULE['<{blockuserinfo}jewelry>nav_c87aacf5673fada1108c9f809d354311'] = 'Fechar sessão';
$_MODULE['<{blockuserinfo}jewelry>nav_d4151a9a3959bdd43690735737034f27'] = 'Inicie sessão na sua conta de cliente';
$_MODULE['<{blockuserinfo}jewelry>nav_b6d4223e60986fa4c9af77ee5f7149c5'] = 'Entrar';
