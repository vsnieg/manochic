{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->
			{if isset($HOOK_FOOTER)}
				<!-- Footer -->
			
			
			{if isset($page_name) && $page_name == 'index'}
				{if Module::isInstalled('an_staticblocks')}
				{Module::getInstanceByName('an_staticblocks')->getBlockContent('home_countdown')}
				<script>
					(function($){
						var countdown = $('#an_countdown');
							if (countdown.length > 0) {
								countdown.countdown(countdown.text(), function(event) {
									$(this).html(
										event.strftime('<div class="days"><span>%D</span> {l s='Days'}</div>'
										+ '<div class="days"><span>%H</span> {l s='Hours'}</div>'
										+ '<div class="days"><span>%M</span> {l s='Minutes'}</div>'
										+ '<div class="days"><span>%S</span> {l s='Seconds'}</div>')
										);
									});
								}
						})(jQuery);
					</script>
				{/if}

				<div class="container">
				
					{if Module::isInstalled('an_staticblocks')}
						<div class="row product-slid">
							{assign var=block value=Module::getInstanceByName('an_staticblocks')->getProductsBlock(1)}
							{if count($block.products)}
							<div class="col-sm-4 mal-s-33">
								<div class="title">{l s='Braslets'}</div>
								{assign var=products value=$block.products}
								<div class="an_products_slider">
									{include file="$tpl_dir./home-product-list.tpl" class='home_products_block' id='an_block1'}
								</div>
							</div>
						{/if}
						{assign var=block value=Module::getInstanceByName('an_staticblocks')->getProductsBlock(2)}
						{if count($block.products)}
							<div class="col-sm-4 mal-s-34">
								<div class="title">{l s='Charms'}</div>
								{assign var=products value=$block.products}
								<div class="an_products_slider">
									{include file="$tpl_dir./home-product-list.tpl" class='home_products_block' id='an_block2'}
								</div>
							</div>
						{/if}
						{assign var=block value=Module::getInstanceByName('an_staticblocks')->getProductsBlock(3)}
						{if count($block.products)}
							<div class="col-sm-4 mal-s-33">
								<div class="title">{l s='Necklaces'}</div>
								{assign var=products value=$block.products}
								<div class="an_products_slider">
									{include file="$tpl_dir./home-product-list.tpl" class='home_products_block' id='an_block3'}
								</div>
							</div>
						{/if}
						</div>
						<script>
							$('.an_products_slider').bxSlider();
						</script>
					{/if}
					
					
					{if Module::isInstalled('an_staticblocks')}
					{Module::getInstanceByName('an_staticblocks')->getBlockContent('brands-block')}
					{/if}

					{if Module::isInstalled('an_staticblocks')}
					{Module::getInstanceByName('an_staticblocks')->getBlockContent('bottom-block')}
					{/if}
				</div>
				
				
				<div class="footer-static-block-3">
				<div class="container">
				{if Module::isInstalled('an_staticblocks')}
				{Module::getInstanceByName('an_staticblocks')->getBlockContent('bottom-block-3')}
				{/if}
				</div>
				</div>
			{/if}	
				
				<div class="second-futer">
					<div class="container">
						<div class="row">
							<div class="col-sm-6 col-md-7">
								<div class="fotter-about">
								{if Module::isInstalled('an_staticblocks')}
								{Module::getInstanceByName('an_staticblocks')->getBlockContent('about-us')}
								{/if}
								</div>
							</div>
							<div class="col-sm-6 col-md-5">
								<div class="newsl-soc">

								
								{if Module::isInstalled('blocknewsletter')}
								{Module::getInstanceByName('blocknewsletter')->hookFooter([])}
								{/if}
								
								{if Module::isInstalled('blocksocial')}
								{Module::getInstanceByName('blocksocial')->hookDisplayFooter([])}
								{/if}
								</div>							
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="footer-container">
					<footer id="footer"  class="container">
						<div class="row">{$HOOK_FOOTER}</div>
					</footer>
						<section class="bottom-footer">
							<div>
							{l s='[1] %3$s %2$s - Ecommerce software by %1$s [/1]' sprintf=['PrestaShop™', 'Y'|date, '©'] tags=['<a class="_blank" href="http://www.prestashop.com">'] nocache}
							</div>
						</section>
				</div><!-- #footer -->
			{/if}
		</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
	</body>
</html>