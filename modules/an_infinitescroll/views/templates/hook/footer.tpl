<div id="infinitescroll" data-auto="{$infinite_loader.auto_load|escape:'htmlall':'UTF-8'}" style="display: none;">
	{if $infinite_loader.auto_load|escape:'htmlall':'UTF-8' == 0}
		<div class="an_button_more_products" style="{$style.button_color|escape:'htmlall':'UTF-8'}{$style.text_button_color|escape:'htmlall'}">{l s='Load more products:' mod='an_infinitescroll'}</div>
	{else}
		<img src="{$infinite_loader.img|escape:'htmlall':'UTF-8'}" />
	{/if}
</div>
