if (location.hash != '') {
	location.hash = '';
}
var infinitescrollBlocklayered = function(params_plus, productList){
	if (typeof params_plus == 'string' && params_plus.indexOf('&p=') != -1) {
		$('.product_list').append(utf8_decode(productList));
		$('.product_list .product_list').each(function(){
			$(this).replaceWith($(this).html());
		});
		$('#infinitescroll').hide();
	} else {
		$('.product_list').replaceWith(utf8_decode(productList));
		$.scrollTo('.product_list', 400);
	}
};
(function(){
	$(document).ready(function(){
		var pagination_next = $('.pagination_next a');
		if (pagination_next.length) {
			$('ul.product_list').after($('#infinitescroll'));

			if (typeof ajaxLoaderOn == 'undefined') {
				var ajaxLoaderOn = 0;
			}
			if (typeof reloadContent != 'undefined') {
				var oldReloadContent = reloadContent.toString();
				var needToRewrite = "$('.product_list').replaceWith(utf8_decode(result.productList));";
				var newReloadContent = oldReloadContent.replace(needToRewrite, "{slideUp = false; infinitescrollBlocklayered(params_plus, result.productList);}");
				newReloadContent = eval(newReloadContent.replace('function reloadContent(', 'reloadContent = function('));
			}
			$(document).scroll(function(){
				if (!ajaxLoaderOn && $('.bottom-pagination-content').visible(true) && pagination_next != false) {
					ajaxLoaderOn = 1;
					var an_autoload = $("#infinitescroll").data("auto");
					var next_url = pagination_next.attr('href');
					//$('.pagination, .product-count').hide();
					if ($('#layered_form').length) {
						//blocklayered...
						if(an_autoload == 1){
							pagination_next = $('.pagination_next a');
							if (pagination_next.children().length > 0) {
								$('#infinitescroll').show();
								pagination_next.children()[0].click();
							}
						} else {
							pagination_next = $('.pagination_next a');
							if (pagination_next.children().length > 0) {
								$('#infinitescroll').show();
							}
						}
					} else {
						//without blocklayered!
						if(an_autoload == 1) {
							$.ajax({
								url: next_url,
								beforeSend: function () {
									$('#infinitescroll').show();
								},
								success: function (html) {
									var next_page = $(html);
									var product_list = next_page.find('ul.product_list');
									$('ul.product_list').append(product_list.html());
									$('ul.display li.selected').click();
									pagination_next = next_page.find('.pagination_next a');
									if (pagination_next.length == 0) {
										pagination_next = false;
									}
								},
								complete: function () {
									$('#infinitescroll').hide();
									ajaxLoaderOn = 0;
								}
							});
						} else {
							$('#infinitescroll').show();
						}
					}
				}
			});
		}
		$('body').on("click", "#infinitescroll", function(){
			var next_url = pagination_next.attr('href');
			if ($('#layered_form').length) {
				//blocklayered...
					pagination_next = $('.pagination_next a');
					if (pagination_next.children().length > 0) {
						pagination_next.children()[0].click();
						$( document ).ajaxSuccess(function( event, request, settings ) {
							if($('.pagination_next a').children().length > 0){
								$('#infinitescroll').show();
							}
						});
					}
			} else {
				$.ajax({
					url: next_url,
					beforeSend: function () {
						$('#infinitescroll').show();
					},
					success: function (html) {
						var next_page = $(html);
						var product_list = next_page.find('ul.product_list');
						$('ul.product_list').append(product_list.html());
						$('ul.display li.selected').click();
						pagination_next = next_page.find('.pagination_next a');
						if (pagination_next.length == 0) {
							pagination_next = false;
						}
					},
					complete: function () {
						if(pagination_next){
							$('#infinitescroll').show();
						} else {
							$('#infinitescroll').hide();
						}
						ajaxLoaderOn = 0;
					}
				});
			}
		})
	});
})(jQuery);