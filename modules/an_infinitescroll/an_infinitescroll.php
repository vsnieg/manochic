<?php

/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class an_infinitescroll extends Module
{

	public function __construct()
	{
		$this->name = 'an_infinitescroll';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Apply Novation';
		$this->module_key = '05412d33e1c4830963b82c29a7258642';

		Module::__construct();

		$this->displayName = $this->l('Catalog Infinite Scroll');
		$this->description = $this->l('Catalog Infinite Scroll');
	}

	public function install()
	{

		Configuration::updateValue("an_img", "loading.gif");
		Configuration::updateValue("file_image", "");
		Configuration::updateValue("an_display_message", 0);
		Configuration::updateValue("an_auto_load", 1);
		Configuration::updateValue("button_color", "#000000");
		Configuration::updateValue("text_button_color", "#ffffff");

		return (parent::install()
			&& $this->registerHook('displayHeader')
			&& $this->registerHook('displayFooter'));
	}

	public function uninstall()
	{
		return (parent::uninstall()
			&& $this->unregisterHook('displayHeader')
			&& $this->unregisterHook('displayFooter'));
	}

	public function hookDisplayHeader($params)
	{
		$this->context->controller->addCSS($this->_path .'views/css/front.css');
		$this->context->controller->addJS($this->_path .'views/js/jquery.visible.min.js');
		$this->context->controller->addJS($this->_path .'views/js/front.js');
	}

	public function hookDisplayFooter($params)
	{
		$this->context->smarty->assign(array(
				'infinite_loader' => array(
					"img" => $this->_path . 'views/img/'.Configuration::get('an_img'),
					"auto_load" => Configuration::get('an_auto_load'),
					"an_display_message" => Configuration::get('an_display_message'),
				),
				'style' => array(
					"button_color" => "background:".Configuration::get('button_color').";",
					"text_button_color" => "color:".Configuration::get('text_button_color').";"
				)
			)
		);
		return $this->display(__FILE__, 'footer.tpl');
	}

	public function getContent()
	{
		$output = null;
		$this->bootstrap = true;

		if (Tools::isSubmit('submit'.$this->name)) {
			if (isset($_FILES) && $_FILES['file_image']['error'] != 4){
				$an_infinitescroll = Tools::getValue('filename');
				if (!$an_infinitescroll
					|| empty($an_infinitescroll)
					|| !Validate::isGenericName($an_infinitescroll)
				)
					$output .= $this->displayError($this->l('Invalid Configuration value'));
				else {
					// Check file
					$type = Tools::strtolower(Tools::substr(strrchr($_FILES['file_image']['name'], '.'), 1));
					$imagesize = @getimagesize($_FILES['file_image']['tmp_name']);
					if (isset($_FILES['file_image']) &&
						isset($_FILES['file_image']['tmp_name']) &&
						!empty($_FILES['file_image']['tmp_name']) &&
						!empty($imagesize) &&
						in_array(
							Tools::strtolower(Tools::substr(strrchr($imagesize['mime'], '/'), 1)), array(
								'jpg',
								'gif',
								'jpeg',
								'png'
							)
						) &&
						in_array($type, array('jpg', 'gif', 'jpeg', 'png'))
					) {
						$errors = array();
						$temp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');

						if ($error = ImageManager::validateUpload($_FILES['file_image']))
							$errors[] = $error;
						elseif (!$temp_name || !move_uploaded_file($_FILES['file_image']['tmp_name'], _PS_MODULE_DIR_.'an_infinitescroll/views/img/' . $_FILES['file_image']['name']))
							return false;
						if (!$errors) {
							Configuration::updateValue('an_img', $_FILES['file_image']['name']);
							$output .= $this->displayConfirmation($this->l('Settings updated'));
						} else {
							$output .= $this->displayError($this->l('Error'));
						}
					} else {
						$output .= $this->displayError($this->l('Error'));
					}
				}
			}
			Configuration::updateValue("an_display_message", (int)Tools::getValue("an_display_message"));
			Configuration::updateValue("an_auto_load", (int)Tools::getValue("an_auto_load"));
			Configuration::updateValue("button_color", Tools::getValue("button_color"));
			Configuration::updateValue("text_button_color", Tools::getValue("text_button_color"));
		}
		return $output.$this->displayForm();
	}

	public function displayForm()
	{
		$default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		$fields_form = array();
		$fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Settings'),
			),
			'input' => array(
				array(
					'type' => 'free',
					'label' => $this->l('Your image:'),
					'name' => 'an_img',
				),
				array(
					'type' => 'file',
					'label' => $this->l('Image preloader:'),
					'name' => 'file_image'
				),
				array(
					'type' => 'switch',
					'label' => $this->l('Display message (more products):'),
					'name' => 'an_display_message',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
				array(
					'type' => 'switch',
					'label' => $this->l('Load automatically:'),
					'name' => 'an_auto_load',
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
				array(
					'type' => 'color',
					'name' => 'button_color',
					'label' => $this->l('Button color style:'),
				),
				array(
					'type' => 'color',
					'name' => 'text_button_color',
					'label' => $this->l('Text button color style:'),
				),

			),
			'submit' => array(
				'title' => $this->l('Save'),
			)
		);

		$helper = new HelperForm();

		$helper->module = $this;
		$helper->name_controller = $this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

		$helper->default_form_language = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;

		$helper->title = $this->displayName;
		$helper->show_toolbar = true;        // false -> remove toolbar
		$helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
		$helper->submit_action = 'submit'.$this->name;
		$helper->toolbar_btn = array(
			'save' =>
				array(
					'desc' => $this->l('Save'),
					'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
						'&token='.Tools::getAdminTokenLite('AdminModules'),
				),
			'back' => array(
				'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
				'desc' => $this->l('Back to list')
			)
		);

		if(Configuration::get('an_img')){
			$helper->fields_value['an_img'] = "<img src='".$this->_path."views/img/".Configuration::get('an_img')."' />";
		}
		$helper->fields_value['an_display_message'] = Configuration::get('an_display_message');
		$helper->fields_value['an_auto_load'] = Configuration::get('an_auto_load');
		$helper->fields_value['button_color'] = Configuration::get('button_color');
		$helper->fields_value['text_button_color'] = Configuration::get('text_button_color');

		return $helper->generateForm($fields_form);
	}
}
