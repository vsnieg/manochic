(function($){
	$(document).ready(function(){
		$('p.payment_module a').live('click', function(e){
			$('p.payment_module a').removeClass('active');
			$(this).addClass('active');

			return false;
		});

		$('a.confirm-order').click(function(){
			var payment = $('p.payment_module a.active:first');
			if (payment.length > 0) {
				window.location.href = payment.attr('href');
			} else {
				var pm_block = $("#opc_payment_methods");
				$('html, body').animate({
			        scrollTop: pm_block.offset().top-100
			    }, 200, function(){
			    	pm_block.stop().prev().animate({color: 'red'}, 100);
			    	if (pm_block.find('a').length > 0) {
					    pm_block.find('a').effect("shake", {distance: 10, times: 1}, 80, function(){
							pm_block.stop().prev().animate({color: 'black'}, 200);
						});
					} else {
						pm_block.stop().prev().animate({color: 'black'}, 200);
					}
			    });
			}
		});

		$('td.delivery_option_name, td.delivery_option_price').live('click', function(){
			$(this).parent().find('input.delivery_option_radio').click();
		});
	});
})(jQuery);
