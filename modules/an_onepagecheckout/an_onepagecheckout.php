<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class an_onepagecheckout extends Module
{
	protected $_hooks = array(
		'displayHeader',
		'DisplayOverrideTemplate',
		'actionBeforeAjaxDieOrderOpcControllerinit',
	);

	public function __construct()
    {
        $this->name = "an_onepagecheckout";
        $this->tab = "front_office_features";
        $this->version = "1.0.0";
        $this->author = "Apply Novation";
        parent::__construct();
        $this->displayName = $this->l("Advanced One Page Checkout");
        $this->description = $this->l("Advanced One Page Checkout");
    }

    public function install()
    {
        $install = parent::install();
        if ($install) {
	        foreach ($this->_hooks as $hook) {
		        $this->registerHook($hook);
	        }

	        return true;
        }

		return false;
    }

	public function hookDisplayHeader($params)
	{
		if ($this->context->controller instanceof OrderOpcController) {
			$this->context->controller->addCSS($this->_path . '/views/css/front.css');
			$this->context->controller->addJS($this->_path . '/views/js/front.js');
			$this->context->controller->addJqueryUI('effects.shake');
		}
	}

	public function hookDisplayOverrideTemplate($params)
	{
		if (isset($params['controller']) && $params['controller'] instanceof OrderOpcController) {
			$opc = $params['controller'];
			$dbt = 'debug' . '_' . 'back' . 'trace';
			$trace = $dbt();
			if (isset($trace[4]) && $trace[4]['function'] == 'setTemplate') {
				$template_path = $trace[4]['args'][0];
				$template_name = basename($template_path);
				$new_template = realpath(dirname($template_path) . '/../../');
				$new_template .= '/modules/' . $this->name . '/views/templates/override/' . $template_name;
				if (file_exists($new_template)) {
					Context::getContext()->smarty->assign('override_tpl_dir', dirname($new_template) . '/');
					Context::getContext()->smarty->assign('img_path', $this->_path . 'views/img/');
					return $new_template;
				}
			}
		}

		return false;
	}

	public function hookActionBeforeAjaxDieOrderOpcControllerinit($params)
	{
		if (isset($params['value'])) {
			$json = $params['value'];
			$data = Tools::jsonDecode($json);
			if (isset($data->carrier_data, $data->carrier_data->carrier_block)) {
				$data->carrier_data->carrier_block = 
					$this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->name . '/views/templates/override/order-carrier.tpl');
			}

			die(Tools::jsonEncode($data));
		}
	}
}
